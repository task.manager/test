﻿namespace Infrastructure.Entity
{
    public interface IWorkSpace
    {
        Guid Id { get; set; }
        string Name { get; set; }
    }
}
