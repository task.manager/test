﻿namespace Infrastructure.Entity
{
    public interface IDeskVisibilityType
    {
        Guid Id { get; set; }
        string Code { get; set; }
    }
}
